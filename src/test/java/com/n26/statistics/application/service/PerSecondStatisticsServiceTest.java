package com.n26.statistics.application.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.n26.statistics.domain.Transaction;
import java.sql.Timestamp;
import java.time.Instant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("")
class PerSecondStatisticsServiceTest {

  private PerSecondStatisticsService serviceUnderTest = new PerSecondStatisticsService();

  @Test
  @DisplayName("")
  void addTransaction() {

    // given
    Transaction transaction =
        Transaction.builder().amount(12).timestamp(Timestamp.from(Instant.now())).build();

    // when
    boolean result = serviceUnderTest.addTransaction(transaction);

    // then
    assertThat(result).isTrue();
  }

  @Test
  @DisplayName("")
  void addTransactionShouldCalculateStatistics() {

    Timestamp timestamp = Timestamp.from(Instant.now().minusSeconds(1));
    // given
    Transaction transaction = Transaction.builder().amount(12).timestamp(timestamp).build();

    // when
    boolean result = serviceUnderTest.addTransaction(transaction);

    // then
    assertThat(serviceUnderTest.getConcurrentSkipListMap().get(1).getSum()).isEqualTo(12);
  }

  @Test
  @DisplayName("")
  void addTransactionShouldCalculateStatisticsMerge() {

    Timestamp timestamp = Timestamp.from(Instant.now().minusSeconds(1));
    // given
    Transaction transaction = Transaction.builder().amount(12).timestamp(timestamp).build();

    // when
    serviceUnderTest.addTransaction(transaction);
    serviceUnderTest.addTransaction(transaction);

    // then
    assertThat(serviceUnderTest.getConcurrentSkipListMap().get(1).getSum()).isEqualTo(24);
  }
}
