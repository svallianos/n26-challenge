package com.n26.statistics.adapter.in.web;

import static com.n26.statistics.shared.ApplicationConfiguration.API_STATISTICS_URL;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import com.n26.statistics.application.port.in.CalculateStatisticsUseCase;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import java.util.DoubleSummaryStatistics;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(GetStatisticsController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestInstance(value = Lifecycle.PER_CLASS)
@DisplayName("The GetStatisticsController should:")
class GetStatisticsControllerShould {

  @MockBean private CalculateStatisticsUseCase calculateStatisticsUseCase;

  @Autowired private MockMvc mockMvc;

  @BeforeAll
  void setUp() {
    RestAssuredMockMvc.mockMvc(mockMvc);
  }

  @Test
  @DisplayName("Fetch the statistics.")
  void fetchStatistics() {

    // given
    given(calculateStatisticsUseCase.calculate()).willReturn(new DoubleSummaryStatistics());

    RestAssuredMockMvc.given()
        .when()
        .get(API_STATISTICS_URL)
        .then()
        .statusCode(200)
        .body("status", is("success"))
        .body("code", is("200 OK"))
        .body("message", is("Statistics from the last 60 seconds successfully retrieved."))
        .body("timestamp", notNullValue());

    then(calculateStatisticsUseCase).should().calculate();
  }
}
