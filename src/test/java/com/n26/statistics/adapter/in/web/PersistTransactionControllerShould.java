package com.n26.statistics.adapter.in.web;

import static com.n26.statistics.shared.ApplicationConfiguration.API_TRANSACTIONS_URL;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;

import com.n26.statistics.application.port.in.PersistTransactionUseCase;
import com.n26.statistics.application.port.in.PersistTransactionUseCase.PersistTransactionCommand;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import java.sql.Timestamp;
import java.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(PersistTransactionController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestInstance(value = Lifecycle.PER_CLASS)
@DisplayName("The PersistTransactionController should:")
class PersistTransactionControllerShould {

  @MockBean private PersistTransactionUseCase persistTransactionUseCase;

  @Autowired private MockMvc mockMvc;

  @BeforeAll
  void setUp() {
    RestAssuredMockMvc.mockMvc(mockMvc);
  }

  @Test
  @DisplayName(
      "Persist a transaction and return 201 when the transaction is not older than 60 seconds.")
  void persistTransactionWhenTransactionIsNewerThan60Seconds() {

    // given
    Timestamp now = Timestamp.from(Instant.now());
    var command = PersistTransactionCommand.builder().amount(12.3).timestamp(now).build();
    willDoNothing().given(persistTransactionUseCase).persist(command);

    RestAssuredMockMvc.given()
        .contentType(ContentType.JSON)
        .body(command)
        .when()
        .post(API_TRANSACTIONS_URL)
        .then()
        .statusCode(201)
        .body("status", is("success"))
        .body("code", is("201 CREATED"))
        .body("message", is("Transaction successfully persisted."))
        .body("timestamp", notNullValue());

    then(persistTransactionUseCase).should().persist(any(PersistTransactionCommand.class));
  }

  @Test
  @DisplayName(
      "Persist a transaction and return 204 when the transaction is older than 60 seconds.")
  void persistTransactionWhenTransactionIsOlderThan60Seconds() {

    // given
    Timestamp oldTimestamp = new Timestamp(1478192204000L);
    var command = PersistTransactionCommand.builder().amount(12.3).timestamp(oldTimestamp).build();
    willDoNothing().given(persistTransactionUseCase).persist(command);

    RestAssuredMockMvc.given()
        .contentType(ContentType.JSON)
        .body(command)
        .when()
        .post(API_TRANSACTIONS_URL)
        .then()
        .statusCode(204);

    then(persistTransactionUseCase).should().persist(any(PersistTransactionCommand.class));
  }
}
