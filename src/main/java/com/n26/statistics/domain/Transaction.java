package com.n26.statistics.domain;

import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;

import java.sql.Timestamp;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public final class Transaction {

  private final double amount;

  private final Timestamp timestamp;

  @Builder
  private Transaction(double amount, Timestamp timestamp) {
    this.amount = amount;
    this.timestamp = timestamp;

    isTrue(amount > 0, "The transaction amount cannot be zero.");
    notNull(timestamp, "The transaction timestamp cannot be null.");
  }

}
