package com.n26.statistics.adapter.in.web;

import static com.n26.statistics.shared.ApplicationConfiguration.API_STATISTICS_URL;

import com.konnektable.ktjsend.ApiSuccessResponse;
import com.n26.statistics.application.port.in.CalculateStatisticsUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(API_STATISTICS_URL)
@RequiredArgsConstructor
final class GetStatisticsController {

  private final CalculateStatisticsUseCase calculateStatisticsUseCase;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public ApiSuccessResponse calculateStatistics() {

    return ApiSuccessResponse.builder()
        .code(HttpStatus.OK)
        .data(calculateStatisticsUseCase.calculate())
        .message("Statistics from the last 60 seconds successfully retrieved.")
        .build();
  }
}
