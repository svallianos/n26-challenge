package com.n26.statistics.adapter.in.web;

import static com.n26.statistics.shared.ApplicationConfiguration.API_TRANSACTIONS_URL;

import com.konnektable.ktjsend.ApiSuccessResponse;
import com.n26.statistics.application.port.in.PersistTransactionUseCase;
import com.n26.statistics.application.port.in.PersistTransactionUseCase.PersistTransactionCommand;
import java.sql.Timestamp;
import java.time.Instant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(API_TRANSACTIONS_URL)
@RequiredArgsConstructor
final class PersistTransactionController {

  private final PersistTransactionUseCase persistTransactionUseCase;

  @PostMapping
  public ResponseEntity<ApiSuccessResponse> persistTransaction(
      @RequestBody PersistTransactionCommand command) {


    boolean isTransactionOlderThan60Seconds =
        command.getTimestamp().before(Timestamp.from(Instant.now().minusSeconds(60)));

    if (isTransactionOlderThan60Seconds) {
      return ResponseEntity.noContent().build();
    }

    persistTransactionUseCase.persist(command);

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(
            ApiSuccessResponse.builder()
                .code(HttpStatus.CREATED)
                .message("Transaction successfully persisted.")
                .build());
  }
}
