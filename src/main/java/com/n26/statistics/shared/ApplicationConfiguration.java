package com.n26.statistics.shared;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplicationConfiguration {

  public static final String API_STATISTICS_URL = "/api/statistics";
  public static final String API_TRANSACTIONS_URL = "/api/transactions";
}
