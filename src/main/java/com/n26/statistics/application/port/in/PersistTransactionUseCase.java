package com.n26.statistics.application.port.in;

import java.sql.Timestamp;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

public interface PersistTransactionUseCase {

  void persist(PersistTransactionCommand command);

  @Value
  @AllArgsConstructor(access = AccessLevel.PRIVATE)
  @Builder
  final class PersistTransactionCommand {

    private final double amount;

    private final Timestamp timestamp;
  }
}
