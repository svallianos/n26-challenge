package com.n26.statistics.application.port.in;

import java.util.DoubleSummaryStatistics;

public interface CalculateStatisticsUseCase {

  DoubleSummaryStatistics calculate();
}
