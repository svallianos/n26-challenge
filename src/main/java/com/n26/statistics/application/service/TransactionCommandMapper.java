package com.n26.statistics.application.service;

import com.n26.statistics.application.port.in.PersistTransactionUseCase.PersistTransactionCommand;
import com.n26.statistics.domain.Transaction;
import java.util.Optional;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
interface TransactionCommandMapper {

  Transaction persistTransactionCommandToTransaction(PersistTransactionCommand command);

  @Named("unwrap")
  default <T> T unwrap(Optional<T> optional) {
    return optional.orElse(null);
  }
}
