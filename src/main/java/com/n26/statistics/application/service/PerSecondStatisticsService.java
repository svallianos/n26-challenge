package com.n26.statistics.application.service;

import com.n26.statistics.domain.Transaction;
import com.n26.statistics.repository.PerSecondStatisticsRepository;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.DoubleSummaryStatistics;
import java.util.concurrent.ConcurrentSkipListMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Getter
public class PerSecondStatisticsService implements PerSecondStatisticsRepository {

  private ConcurrentSkipListMap<Integer, DoubleSummaryStatistics> concurrentSkipListMap =
      new ConcurrentSkipListMap<>();

  @Override
  public boolean addTransaction(Transaction transaction) {

    DoubleSummaryStatistics transactionStatistics =
        new DoubleSummaryStatistics(
            1, transaction.getAmount(), transaction.getAmount(), transaction.getAmount());

    concurrentSkipListMap.merge(
        calculateSlot(transaction),
        transactionStatistics,
        this::combineDoubleSummaryStatistics);
    return true;
  }

  private DoubleSummaryStatistics combineDoubleSummaryStatistics(
      DoubleSummaryStatistics currentStatistics, DoubleSummaryStatistics transactionStatistic) {
    currentStatistics.combine(transactionStatistic);
    return currentStatistics;
  }

  private Integer calculateSlot(Transaction transaction) {
    int timeDiff = (int) (Timestamp.from(Instant.now()).getTime() - transaction.getTimestamp().getTime()) / 1000;
    return timeDiff % 6;
  }
}
