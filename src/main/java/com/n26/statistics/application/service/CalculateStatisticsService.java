package com.n26.statistics.application.service;

import com.n26.statistics.application.port.in.CalculateStatisticsUseCase;
import com.n26.statistics.domain.Transaction;
import com.n26.statistics.repository.PerSecondStatisticsRepository;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.DoubleSummaryStatistics;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CalculateStatisticsService implements CalculateStatisticsUseCase {

  private final PerSecondStatisticsRepository repository;

  @Override
  public DoubleSummaryStatistics calculate() {

    var sixtySecondsAgo = Timestamp.from(Instant.now().minusSeconds(60));
    var now = Timestamp.from(Instant.now());

    return new DoubleSummaryStatistics();
  }
}
