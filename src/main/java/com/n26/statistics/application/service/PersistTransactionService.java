package com.n26.statistics.application.service;

import com.n26.statistics.application.port.in.PersistTransactionUseCase;
import com.n26.statistics.repository.PerSecondStatisticsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
final class PersistTransactionService implements PersistTransactionUseCase {

  private final TransactionCommandMapper mapper;
  private final PerSecondStatisticsRepository repository;

  @Override
  public void persist(PersistTransactionCommand command) {

    repository.addTransaction(mapper.persistTransactionCommandToTransaction(command));
  }
}
