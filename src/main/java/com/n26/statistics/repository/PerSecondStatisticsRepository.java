package com.n26.statistics.repository;

import com.n26.statistics.domain.Transaction;
import java.sql.Timestamp;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface PerSecondStatisticsRepository {

  boolean addTransaction(Transaction transaction);

}
