package com.n26.statistics;

import static org.springframework.boot.SpringApplication.run;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StatisticsApplication {

  public static void main(String[] args) {
    run(StatisticsApplication.class, args);
  }
}
